<?php
declare(strict_types=1);

use JanHelke\CalendarDemonstration\Command\CreateEntriesCommand;

return [
    'calendar:create' => [
        'class' => CreateEntriesCommand::class,
        'schedulable' => false,
    ],
];
