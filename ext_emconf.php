<?php

$EM_CONF['calendar_demonstration'] = [
    'title' => 'Calendar Demonstration',
    'description' => 'This extensions provides automatically generated entries for the calendar extension.',
    'category' => 'plugin',
    'version' => '1.0.0',
    'state' => 'stable',
    'author' => 'Jan Helke',
    'author_email' => 'calendar@typo3.helke.de',
    'constraints' => [
        'depends' => [
            'calendar_foundation' => '1.0 - 1.999'
        ],
    ]
];
