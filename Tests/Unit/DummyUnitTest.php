<?php
declare(strict_types=1);

namespace JanHelke\CalendarDemonstration\Tests\Unit\Service;

use TYPO3\TestingFramework\Core\Unit\UnitTestCase;

/**
 * Dummy test
 */
class DummyUnitTest extends UnitTestCase
{

    /**
     * To make the automatic testing chain running, we need at least a dummy test
     * @todo Remove this whole class together with your first real tests.
     */
    public function testDummyFunction(): void
    {
        self::assertEquals(1, 1);
    }
}
