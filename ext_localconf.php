<?php

(static function () {
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks'][\JanHelke\CalendarDemonstration\Task\CreationTask::class] = [
        'extension' => 'calendar_demonstration',
        'title' => 'LLL:EXT:calendar_demonstration/Resources/Private/Language/locallang.xlf:creation_task.name',
        'description' => 'LLL:EXT:calendar_demonstration/Resources/Private/Language/locallang.xlf:creation_task.description',
        'additionalFields' => \JanHelke\CalendarDemonstration\Task\CreationTask::class
    ];
})();
