<?php
declare(strict_types=1);

namespace JanHelke\CalendarDemonstration\Command;

use JanHelke\CalendarDemonstration\Service\CreateEntryService;
use JanHelke\CalendarFoundation\Service\EventService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;

/**
 * Class CreateEntriesCommand
 */
class CreateEntriesCommand extends Command
{

    /**
     * Configure the command by defining the name, options and arguments
     */
    protected function configure(): void
    {
        $this
            ->setName('calendar:create')
            ->setDescription('Creates all calendar_framework entries.')
            ->setHelp('Creates all entries of the table tx_calendar_entry combined with the recurrences and creates the events stored in the table tx_calendar_event.');
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @see setCode()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln([
            'Creates entries for the calendar_framework.',
            '============',
        ]);
        $events = 0;
        $entries = GeneralUtility::makeInstance(ObjectManager::class)->get(CreateEntryService::class)->createEntries();
        if ($entries > 0) {
            $events = GeneralUtility::makeInstance(ObjectManager::class)->get(EventService::class)->createAndSaveEventIndex();
        }

        $output->writeln([
            'Done. ' . $entries . ' entries and ' . $events . ' events were created and stored in the database.',
        ]);
        return 0;
    }
}
