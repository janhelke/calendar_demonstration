<?php
declare(strict_types=1);

namespace JanHelke\CalendarDemonstration\Task;

use JanHelke\CalendarDemonstration\Service\CreateEntryService;
use JanHelke\CalendarFoundation\Service\EventService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Scheduler\Task\AbstractTask;

/**
 * Class CreationTask
 */
class CreationTask extends AbstractTask
{
    /**
     * This is the main method that is called when a task is executed
     * It MUST be implemented by all classes inheriting from this one
     * Note that there is no error handling, errors and failures are expected
     * to be handled and logged by the client implementations.
     * Should return TRUE on successful execution, FALSE on error.
     *
     * @return bool Returns TRUE on successful execution, FALSE on error
     */
    public function execute(): bool
    {
        $events = 0;
        $entries = GeneralUtility::makeInstance(ObjectManager::class)->get(CreateEntryService::class)->createEntries();
        if ($entries > 0) {
            $events = GeneralUtility::makeInstance(ObjectManager::class)->get(EventService::class)->createAndSaveEventIndex();
        }

        return (bool)$events;
    }
}
