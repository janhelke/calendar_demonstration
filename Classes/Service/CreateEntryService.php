<?php
declare(strict_types=1);

namespace JanHelke\CalendarDemonstration\Service;

use DateTime;
use Exception;
use JanHelke\CalendarFoundation\Utility\CalculateDateTimeUtility;
use PDO;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\Localization\LanguageService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\StringUtility;
use TYPO3\CMS\Core\Utility\VersionNumberUtility;

/**
 * Class CreateEntryService
 */
class CreateEntryService
{
    protected array $data = [];

    protected int $storagePid = 0;

    protected DataHandler $dataHandler;

    protected \DateInterval $dateDifference;

    /**
     * CreateEventService constructor.
     * @param DataHandler $dataHandler
     */
    public function __construct(
        DataHandler $dataHandler
    ) {
        $this->initializeFakeAdminUser();
        $this->initializeFakeLanguageService();
        $this->dataHandler = $dataHandler;

        $today = CalculateDateTimeUtility::calculateStartOfDay(new DateTime());
        $referenceDate = CalculateDateTimeUtility::calculateStartOfDay(new DateTime('2020-02-23'));
        $this->dateDifference = $referenceDate->diff($today);
    }

    public function createEntries(): int
    {
        $this->removeDemonstrationEvents();
        $this->storagePid = StringUtility::getUniqueId('NEW');

        $this->data = [
            'pages' => [
                $this->storagePid => [
                    'title' => 'calendar_demonstration_storage',
                    'pid' => 0,
                    'doktype' => 254,
                    'hidden' => 0
                ]
            ],
            'tx_calendar_calendar' => [],
            'tx_calendar_entry' => [],
            'tx_calendar_recurrence' => [],
            'tx_calendar_deviation' => [],
            'tx_calendar_exception' => [],
            'tx_calendar_exception_group' => [],
        ];

        foreach ($this->entries as $event) {
            $newEntryUid = StringUtility::getUniqueId('NEW');
            $this->data['tx_calendar_entry'][$newEntryUid] = $this->initializeEntry($event);

            if (!empty($event['recurrence'])) {
                $recurrenceUid = StringUtility::getUniqueId('NEW');
                $recurrence = $this->initializeRecurrenceEntry($event['recurrence']);
                $recurrence['entry'] = $newEntryUid;
                $this->data['tx_calendar_recurrence'][$recurrenceUid] = $recurrence;
            }
        }

        $this->dataHandler->start($this->data, []);
        $this->dataHandler->process_datamap();

        return count($this->data['tx_calendar_entry']);
    }

    /**
     * @param array $calEntry
     * @return array
     */
    protected function initializeEntry(array $calEntry): array
    {
        return [
            'pid' => $this->storagePid,
            'tstamp' => time(),
            'crdate' => time(),
            'title' => $calEntry['title'],
            'teaser' => $calEntry['teaser'],
            'description' => $calEntry['description'] ?? '',
            'start' => $this->createDateTime($calEntry['start']),
            'end' => $this->createDateTime($calEntry['end']),
        ];
    }

    /**
     * @param array $calEntry
     * @return array
     */
    protected function initializeRecurrenceEntry(array $calEntry): array
    {
        return [
            'pid' => $this->storagePid,
            'tstamp' => time(),
            'crdate' => time(),
            'frequency' => $calEntry['frequency'] ?? 0,
            'interval' => $calEntry['interval'] ?? 0,
            'by_day_of_week' => $calEntry['by_day_of_week'] ?? 0,
            'by_day_index' => $calEntry['by_day_index'] ?? 0,
            'by_day_day' => $calEntry['by_day_day'] ?? 0,
            'by_day_of_month' => $calEntry['by_day_of_month'] ?? 0,
            'until_date' => $calEntry['until_date'] ?? null,
            'until_recurrence_amount' => $calEntry['until_recurrence_amount'] ?? 0
        ];
    }

    /**
     * @param string $dateTime
     * @return string
     * @throws Exception
     */
    protected function createDateTime(string $dateTime): string
    {
        return DateTime::createFromFormat('Y-m-d H:i:s', $dateTime)
            ->add($this->dateDifference)
            ->format('Y-m-d H:i:s');
    }

    /**
     * With this function the calendar demonstration storage will be deleted together with all entries in it.
     */
    public function removeDemonstrationEvents(): void
    {
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('pages');
        $pageUid = $queryBuilder
            ->select('uid')
            ->from('pages')
            ->where(
                $queryBuilder->expr()->eq(
                    'title',
                    $queryBuilder->createNamedParameter('calendar_demonstration_storage', PDO::PARAM_STR)
                )
            )
            ->execute()
            ->fetch(0)['uid'];

        if (!empty($pageUid)) {
            $commandMap['pages'][$pageUid]['delete'] = 1;
            $this->dataHandler->start([], $commandMap);
            $this->dataHandler->process_cmdmap();
        }
    }

    /**
     * The admin user is required to defined workspace state when working
     * with DataHandler
     */
    protected function initializeFakeAdminUser(): void
    {
        $fakeAdminUser = GeneralUtility::makeInstance(BackendUserAuthentication::class);
        $fakeAdminUser->user = ['uid' => 0, 'username' => '_migration_', 'admin' => 1];
        $fakeAdminUser->id = '0';
        $fakeAdminUser->workspace = 0;
        $GLOBALS['BE_USER'] = $fakeAdminUser;
    }

    protected array $entries = [
        [
            'title' => 'Today',
            'teaser' => "This is the day you're looking at this calendar.",
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque vitae nulla nibh. Quisque dapibus mi vel ornare malesuada. Nullam in felis at lorem pellentesque faucibus at sed lectus. Curabitur luctus orci ac rhoncus imperdiet. Vivamus malesuada gravida felis non imperdiet. Phasellus nisl sapien, porttitor nec pretium at, pretium at libero. Nulla rhoncus blandit molestie. Vestibulum a aliquet leo. Maecenas suscipit interdum massa et auctor. Etiam libero nisl, posuere at imperdiet quis, commodo ut enim. Duis tincidunt quam nec neque iaculis hendrerit. Sed accumsan, nibh non ultricies hendrerit, nisl eros rhoncus eros, quis efficitur purus sem at tortor. Ut quam ante, efficitur sed tortor ac, molestie sodales dolor. Mauris vel purus vitae tortor imperdiet egestas quis id mi. Maecenas at lorem vel orci volutpat porttitor. Mauris elementum nisl id erat posuere, id elementum libero auctor.',
            'start' => '2020-02-23 12:00:00',
            'end' => '2020-02-23 15:00:00',
            'recurrence' => [],
        ],
        [
            'title' => 'First entry',
            'teaser' => 'This is the very first entry in this demonstration.',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque vitae nulla nibh. Quisque dapibus mi vel ornare malesuada. Nullam in felis at lorem pellentesque faucibus at sed lectus. Curabitur luctus orci ac rhoncus imperdiet. Vivamus malesuada gravida felis non imperdiet. Phasellus nisl sapien, porttitor nec pretium at, pretium at libero. Nulla rhoncus blandit molestie. Vestibulum a aliquet leo. Maecenas suscipit interdum massa et auctor. Etiam libero nisl, posuere at imperdiet quis, commodo ut enim. Duis tincidunt quam nec neque iaculis hendrerit. Sed accumsan, nibh non ultricies hendrerit, nisl eros rhoncus eros, quis efficitur purus sem at tortor. Ut quam ante, efficitur sed tortor ac, molestie sodales dolor. Mauris vel purus vitae tortor imperdiet egestas quis id mi. Maecenas at lorem vel orci volutpat porttitor. Mauris elementum nisl id erat posuere, id elementum libero auctor.',
            'start' => '2020-01-01 08:00:00',
            'end' => '2020-01-01 10:00:00',
            'recurrence' => [],
        ],
        [
            'title' => 'Last entry',
            'teaser' => 'This is the very first entry in this demonstration.',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque vitae nulla nibh. Quisque dapibus mi vel ornare malesuada. Nullam in felis at lorem pellentesque faucibus at sed lectus. Curabitur luctus orci ac rhoncus imperdiet. Vivamus malesuada gravida felis non imperdiet. Phasellus nisl sapien, porttitor nec pretium at, pretium at libero. Nulla rhoncus blandit molestie. Vestibulum a aliquet leo. Maecenas suscipit interdum massa et auctor. Etiam libero nisl, posuere at imperdiet quis, commodo ut enim. Duis tincidunt quam nec neque iaculis hendrerit. Sed accumsan, nibh non ultricies hendrerit, nisl eros rhoncus eros, quis efficitur purus sem at tortor. Ut quam ante, efficitur sed tortor ac, molestie sodales dolor. Mauris vel purus vitae tortor imperdiet egestas quis id mi. Maecenas at lorem vel orci volutpat porttitor. Mauris elementum nisl id erat posuere, id elementum libero auctor.',
            'start' => '2020-03-30 23:00:00',
            'end' => '2020-03-30 23:59:59',
            'recurrence' => [],
        ],
        [
            'title' => 'Ballroom dancing',
            'teaser' => 'This happens every friday.',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque vitae nulla nibh. Quisque dapibus mi vel ornare malesuada. Nullam in felis at lorem pellentesque faucibus at sed lectus. Curabitur luctus orci ac rhoncus imperdiet. Vivamus malesuada gravida felis non imperdiet. Phasellus nisl sapien, porttitor nec pretium at, pretium at libero. Nulla rhoncus blandit molestie. Vestibulum a aliquet leo. Maecenas suscipit interdum massa et auctor. Etiam libero nisl, posuere at imperdiet quis, commodo ut enim. Duis tincidunt quam nec neque iaculis hendrerit. Sed accumsan, nibh non ultricies hendrerit, nisl eros rhoncus eros, quis efficitur purus sem at tortor. Ut quam ante, efficitur sed tortor ac, molestie sodales dolor. Mauris vel purus vitae tortor imperdiet egestas quis id mi. Maecenas at lorem vel orci volutpat porttitor. Mauris elementum nisl id erat posuere, id elementum libero auctor.',
            'start' => '2020-01-23 10:00:00',
            'end' => '2020-01-23 12:00:00',
            'recurrence' => [
                'frequency' => 2,
                'interval' => 1,
                'by_day_of_week' => 32,
                'by_day_index' => 0,
                'by_day_day' => 0,
                'by_day_of_month' => 0,
                'until_date' => null,
                'until_recurrence_amount' => 12
            ],
        ],
        [
            'title' => 'Healthy cooking with chocolate',
            'teaser' => 'This happens every second tuesday.',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque vitae nulla nibh. Quisque dapibus mi vel ornare malesuada. Nullam in felis at lorem pellentesque faucibus at sed lectus. Curabitur luctus orci ac rhoncus imperdiet. Vivamus malesuada gravida felis non imperdiet. Phasellus nisl sapien, porttitor nec pretium at, pretium at libero. Nulla rhoncus blandit molestie. Vestibulum a aliquet leo. Maecenas suscipit interdum massa et auctor. Etiam libero nisl, posuere at imperdiet quis, commodo ut enim. Duis tincidunt quam nec neque iaculis hendrerit. Sed accumsan, nibh non ultricies hendrerit, nisl eros rhoncus eros, quis efficitur purus sem at tortor. Ut quam ante, efficitur sed tortor ac, molestie sodales dolor. Mauris vel purus vitae tortor imperdiet egestas quis id mi. Maecenas at lorem vel orci volutpat porttitor. Mauris elementum nisl id erat posuere, id elementum libero auctor.',
            'start' => '2020-01-23 10:00:00',
            'end' => '2020-01-23 12:00:00',
            'recurrence' => [
                'frequency' => 2,
                'interval' => 2,
                'by_day_of_week' => 4,
                'by_day_index' => 0,
                'by_day_day' => 0,
                'by_day_of_month' => 0,
                'until_date' => null,
                'until_recurrence_amount' => 12
            ],
        ],
        [
            'title' => 'TYPO3 Usergroup Hamburg',
            'teaser' => 'This happens every last wednesday of each month.',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque vitae nulla nibh. Quisque dapibus mi vel ornare malesuada. Nullam in felis at lorem pellentesque faucibus at sed lectus. Curabitur luctus orci ac rhoncus imperdiet. Vivamus malesuada gravida felis non imperdiet. Phasellus nisl sapien, porttitor nec pretium at, pretium at libero. Nulla rhoncus blandit molestie. Vestibulum a aliquet leo. Maecenas suscipit interdum massa et auctor. Etiam libero nisl, posuere at imperdiet quis, commodo ut enim. Duis tincidunt quam nec neque iaculis hendrerit. Sed accumsan, nibh non ultricies hendrerit, nisl eros rhoncus eros, quis efficitur purus sem at tortor. Ut quam ante, efficitur sed tortor ac, molestie sodales dolor. Mauris vel purus vitae tortor imperdiet egestas quis id mi. Maecenas at lorem vel orci volutpat porttitor. Mauris elementum nisl id erat posuere, id elementum libero auctor.',
            'start' => '2020-01-23 10:00:00',
            'end' => '2020-01-23 12:00:00',
            'recurrence' => [
                'frequency' => 3,
                'interval' => 1,
                'by_day_of_week' => 4,
                'by_day_index' => -1,
                'by_day_day' => 4,
                'by_day_of_month' => 0,
                'until_date' => null,
                'until_recurrence_amount' => 3
            ],
        ],
        [
            'title' => 'Visit at grannies',
            'teaser' => 'Overnight event',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque vitae nulla nibh. Quisque dapibus mi vel ornare malesuada. Nullam in felis at lorem pellentesque faucibus at sed lectus. Curabitur luctus orci ac rhoncus imperdiet. Vivamus malesuada gravida felis non imperdiet. Phasellus nisl sapien, porttitor nec pretium at, pretium at libero. Nulla rhoncus blandit molestie. Vestibulum a aliquet leo. Maecenas suscipit interdum massa et auctor. Etiam libero nisl, posuere at imperdiet quis, commodo ut enim. Duis tincidunt quam nec neque iaculis hendrerit. Sed accumsan, nibh non ultricies hendrerit, nisl eros rhoncus eros, quis efficitur purus sem at tortor. Ut quam ante, efficitur sed tortor ac, molestie sodales dolor. Mauris vel purus vitae tortor imperdiet egestas quis id mi. Maecenas at lorem vel orci volutpat porttitor. Mauris elementum nisl id erat posuere, id elementum libero auctor.',
            'start' => '2020-02-23 20:00:00',
            'end' => '2020-02-24 06:00:00',
            'recurrence' => [
                'frequency' => 1,
                'interval' => 1,
                'by_day_of_week' => 32,
                'by_day_index' => 0,
                'by_day_day' => 6,
                'by_day_of_month' => 0,
                'until_date' => null,
                'until_recurrence_amount' => 0
            ],
        ],
    ];

    protected function initializeFakeLanguageService(): void
    {
        if (VersionNumberUtility::convertVersionNumberToInteger('TYPO3_version') >= 10_000_000) {
            $GLOBALS['LANG'] = LanguageService::create('en');
        } elseif (VersionNumberUtility::convertVersionNumberToInteger('TYPO3_version') >= 9_002_000) {
            $GLOBALS['LANG'] = new LanguageService();
        } else {
            $GLOBALS['LANG'] = new LanguageService();
        }
    }
}
